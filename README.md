# README #
Small webserver that replies to requests for files.

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.


## Author: Sam Peters, speters8@uoregon.edu ##
